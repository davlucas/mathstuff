#!/usr/bin/env python
"""
Multiple Math things
"""
import sys
from tkinter import Tk, Frame
from Modules import suitevalue, listvalue, pgvalue


class App:
    """
    Main App
    """

    def __init__(self):
        """Runing all the functions"""
        self.root = Tk()
        self.create_frames()
        self.init_calcul()
        self.root.mainloop()

    def create_frames(self):
        """create frames"""
        self.frameup = Frame(self.root)
        self.frameup.pack()
        self.framemiddle = Frame(self.root)
        self.framemiddle.pack()
        self.framedown = Frame(self.root)
        self.framedown.pack()

    def init_calcul(self):
        """
        run the instances
        """
        print("Pi frame")
        suitevalue.Pi(self.frameup)
        print("....initialized")
        print("Euler  frame")
        suitevalue.Euler(self.frameup)
        print("....initialized")
        print("Prime frame")
        listvalue.Prime(self.framemiddle)
        print("....initialized")
        print("Palindrom frame")
        listvalue.Palindrom(self.framemiddle)
        print("....initialized")
        print("Prime plindrom frame")
        listvalue.PalinAndPrime(self.framemiddle)
        print("....initialized")
        print("Syracuse frame")
        suitevalue.Syracuse(self.frameup)
        print("....initialized")
        pgvalue.Pgcd(self.framedown)
        pgvalue.Ppcm(self.framedown)


def main(_args):
    """
    main
    """
    App()
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
