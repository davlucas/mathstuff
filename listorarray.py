#!/usr/bin/env python
from tkinter import Tk,StringVar,LabelFrame,Entry,Label,Button,Listbox
import math
import numpy as np
import time

def calculpiarray():
    time_start= time.time()
    array=np.arange(1,10000000)
    pi=0
    x=1
    i=0
    precision = 0.0
    for element in np.nditer(array):
        if precision<=99.99999:
            deltapi=(1/x)-(1/(x+2))
            pi+=4*deltapi
            x+=4
            i+=1
            precision=100*(pi/(math.pi))
        else:
            break
    print("{} : {} in {} secs".format("%.20f" % pi, "%.6f" % precision,"%.5f" % float(time.time()-time_start)))

def calculpilist():
    time_start= time.time()
    list=[x for x in range(100000000)]
    pi=0
    x=1
    i=0
    precision = 0.0
    for element in list:
        if precision<=99.99999:
            deltapi=(1/x)-(1/(x+2))
            pi+=4*deltapi
            x+=4
            i+=1
            precision=100*(pi/(math.pi))
        else:
            break
    print("{} : {} in {} secs".format("%.20f" % pi,"%.6f" % precision,"%.5f" % float(time.time()-time_start)))


print (" Calcul de pi à 99.9999%")


print("Avec des array")
calculpiarray()
print("avec des list")
calculpilist()
