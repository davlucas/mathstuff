#!/usr/bin/env py:

import sys
#if n is not odd, n+1 = 3n+1
#if n is odd : n+1=n/2
def syracuse(x):
    orix=x
    syra=[]
    xmax=x
    iteration=0
    while x != 1:
        if x % 2 ==0:
            x=int(x/2)

        else:
            x=(3*x+1)
            if x > xmax:
                xmax=x
        iteration+=1
    return[orix,xmax,iteration]
x= int(sys.argv[1])
table=range(1,x)
sytable=[]
altitude=[]
duree=[]
for i in table:
    a=syracuse(i)
    altitude.append(a[1])
    duree.append(a[2])

print("altitude max: \t {} \t pour: \t {}".format(max(altitude),altitude.index(max(altitude))))
print("duree max: \t {} \t pour: \t {}".format(max(duree),duree.index(max(duree))))
