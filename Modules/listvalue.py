from tkinter import (
    StringVar,
    LabelFrame,
    Label,
    Entry,
    Button,
    Listbox,
    Frame,
    CENTER,
    Scrollbar,
)
import math
import time
from itertools import count, islice


class CalculListe:
    """
    Calcul with a list
    """

    def __init__(self, root):
        """
        init vars
        """
        self.root = root
        self.low_value = StringVar()
        self.high_value = StringVar()
        self.number=StringVar()
        self.time=StringVar()
        self.draw_widget()
        self.tune_widget()

    def tune_widget(self):
        """
        for pylint beauty
        """

    def get_list(self):
        """
        return null as method on subclasses are called
        """

    def draw_widget(self):
        """
        To draw GUI
        """
        self.frame = LabelFrame(self.root, width=50,fg='white',bg="#990000")
        label_low = Label(self.frame, text="Enter lowest value", fg='white',bg="#990000")
        entry_low = Entry(self.frame, width=10, textvariable=self.low_value)
        label_high = Label(
            self.frame, text="Enter Highest value", fg='white',bg="#990000"
        )
        entry_high = Entry(self.frame, width=10, textvariable=self.high_value)
        button = Button(
            self.frame, text="submit", fg='white',bg="#990000", command=self.get_list
        )
        Label(self.frame, fg='white',bg="#990000",text="Number of results").grid(row=3,column=0)
        Entry(self.frame,width=7,textvariable=self.number).grid(row=3,column=1)
        framelist = Frame(self.frame)
        frametime=Frame(self.frame,bg="#990000")
        self.list_result = Listbox(
            framelist, height=7, bg="white", fg="#990000", justify=CENTER
        )
        scrollbar = Scrollbar(
            framelist,
            orient="vertical",
            bg="#990000",
            command=self.list_result.yview,
        )
        scrollbar.grid(column=2, row=0, sticky="ns", rowspan=10)
        self.list_result.config(yscrollcommand=scrollbar.set)
        Label(frametime,text='Time',bg="#990000").grid(column=0,row=0)
        Entry(frametime,textvariable=self.time,fg="#990000",width=8,bg='white').grid(column=1,row=0)
        Label(frametime,text='secs',bg="#990000").grid(column=2,row=0)
        label_low.grid(column=0, row=0)
        label_high.grid(column=0, row=1)
        entry_low.grid(column=1, row=0)
        entry_high.grid(column=1, row=1)
        button.grid(column=0, row=2, columnspan=2)
        framelist.grid(column=0, row=4, columnspan=2)
        frametime.grid(column=0, row=5, columnspan=2)
        self.list_result.grid(column=0, row=0)
        self.time.set(0)


class Prime(CalculListe):
    """
    Prime numbers
    """

    def tune_widget(self):
        self.frame.config(text="Prime numbers")
        self.frame.grid(column=0, row=1)

    def get_list(self):
        time_start = time.time()
        self.list_result.delete(0, "end")
        liste = range(int(self.low_value.get()), int(self.high_value.get()))
        result_list = [
            x
            for x in liste
            if x > 1
            and all(x % i for i in islice(count(2), int(math.sqrt(x) - 1)))
        ]
        self.list_result.insert("end", *result_list)
        self.number.set(len(result_list))
        self.time.set("%.4f" % float(time.time() - time_start))


class Palindrom(CalculListe):
    """
    Display Palindroms
    """

    def tune_widget(self):
        self.frame.config(text="Palindrom numbers")
        self.frame.grid(column=1, row=1)

    def get_list(self):
        time_start = time.time()
        self.list_result.delete(0, "end")
        liste = range(int(self.low_value.get()), int(self.high_value.get()))
        result_list = [x for x in liste if str(x) == str(x)[::-1]]
        self.list_result.insert("end", *result_list)
        self.number.set(format(len(result_list))
        )
        self.time.set("%.4f" % float(time.time() - time_start))

class PalinAndPrime(CalculListe):
    """
    Display prime numbers which are Palindroms
    """

    def tune_widget(self):
        self.frame.config(text="Prime Palindroms")
        self.frame.grid(column=2, row=1)

    def get_list(self):
        time_start = time.time()
        self.list_result.delete(0, "end")
        liste = range(int(self.low_value.get()), int(self.high_value.get()))
        tmplist = [x for x in liste if str(x) == str(x)[::-1]]
        result_list = [
            x
            for x in tmplist
            if x > 1
            and all(x % i for i in islice(count(2), int(math.sqrt(x) - 1)))
        ]
        self.list_result.insert("end", *result_list)
        self.number.set(len(result_list))
        self.time.set("%.4f" % float(time.time() - time_start))
