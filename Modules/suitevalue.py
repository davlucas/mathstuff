from tkinter import StringVar, LabelFrame, Label, Entry, Button
import math
import time


class CalculSuite:
    """
    Parent class to define vars
    for a value calculated after suite_calcul
    """

    def __init__(self, root):
        """
        initialize all the variables
        """
        self.root = root
        self.reached_precision = StringVar()
        self.wanted_precise = StringVar()
        self.reached_value = StringVar()
        self.reached_iteration = StringVar()
        self.timer = StringVar()
        self.init = 0
        self.value = 0
        self.iteration = 0
        self.suite_calcul = 1
        self.time_start = time.time()
        self.calc_value = 0
        self.place_widget()

    def calcul_value(self):
        """
        check subclass
        """

    def tune_widget(self):
        """
        check subclass
        """

    def place_widget(self):
        """
        To display original GUI
        """
        self.frame = LabelFrame(self.root, fg="white", bg="black")
        wanted_precise_label = Label(
            self.frame, text="Enter precision in %", fg="white", bg="black"
        )
        entry = Entry(self.frame, textvariable=self.wanted_precise)
        button = Button(
            self.frame, text="submit", bg="#333333", command=self.calcul_value
        )
        label_reached_value = Label(
            self.frame, text="Reached value", fg="white", bg="black"
        )
        entry_reached_value = Entry(
            self.frame, textvariable=self.reached_value
        )
        label_iteration = Label(
            self.frame, text="after (iterations):", fg="white", bg="black"
        )
        entry_iteration = Entry(
            self.frame, textvariable=self.reached_iteration
        )
        label_precision = Label(
            self.frame, text="Precision", fg="white", bg="black"
        )
        entry_precision = Entry(
            self.frame, textvariable=self.reached_precision
        )
        timer_label = Label(self.frame, text="in", fg="white", bg="black")
        timer_entry = Entry(self.frame, textvariable=self.timer)
        self.reached_iteration.set("0")
        self.reached_precision.set("0.0")
        self.timer.set("0 secs")
        wanted_precise_label.grid(column=0, row=0)
        entry.grid(column=1, row=0)
        button.grid(column=0, row=1, columnspan=2)
        entry_reached_value.grid(column=1, row=3)
        label_reached_value.grid(column=0, row=3)
        label_iteration.grid(column=0, row=2)
        label_precision.grid(column=0, row=4)
        timer_label.grid(column=0, row=5)
        entry_iteration.grid(column=1, row=2)
        entry_precision.grid(column=1, row=4)
        timer_entry.grid(column=1, row=5)
        self.tune_widget()


class Euler(CalculSuite):
    """
    Calculation of Euler number
    """

    def tune_widget(self):
        self.frame.config(text="Calcul de e")
        self.frame.grid(column=1, row=0)
        self.reached_value.set("0")

    def calcul_value(self):
        if not self.init:
            if float(self.wanted_precise.get()) <= 100.0:
                self.init = 1
                self.root.after_idle( self.calcul_value)
                self.time_start = time.time()
                self.calc_value += 1
            else:
                self.wanted_precise.set("max:100")
        else:
            precision = float(self.wanted_precise.get())
            self.iteration += 2
            self.calc_value += 1 / math.factorial(self.suite_calcul)
            self.suite_calcul += 1
            reached_precisionvar = 100 * self.calc_value / math.e
            self.reached_value.set("%.20f" % self.calc_value)
            self.reached_iteration.set(self.iteration)
            self.reached_precision.set(
                "{} %".format("%.5f" % reached_precisionvar)
            )
            self.timer.set(
                "{} secs".format("%.4f" % float(time.time() - self.time_start))
            )
            if reached_precisionvar <= precision:
                self.root.after_idle( self.calcul_value)
            if reached_precisionvar >= precision:
                self.suite_calcul = 1
                self.iteration = 0
                self.calc_value = 0
                self.init = 0


class Pi(CalculSuite):
    """
    Display pi values
    """

    def tune_widget(self):
        self.frame.config(text="Calcul de pi")
        self.frame.grid(column=0, row=0)
        self.reached_value.set("0")

    def calcul_value(self):
        """
        Display details on pi Calculation
        """
        if not self.init:
            if float(self.wanted_precise.get()) < 100.0:
                self.init = 1
                self.root.after_idle( self.calcul_value)
                self.time_start = time.time()
            else:
                self.wanted_precise.set("max(excl):100")
        else:
            precision = float(self.wanted_precise.get())
            self.iteration += 2
            delta_calc_pi = 1 / (self.suite_calcul) - 1 / (
                self.suite_calcul + 2
            )
            self.calc_value += 4 * delta_calc_pi
            self.suite_calcul += 4
            reached_precision = 100 * self.calc_value / math.pi
            self.reached_value.set("%.20f" % self.calc_value)
            self.reached_iteration.set(self.iteration)
            self.reached_precision.set(
                "{} %".format("%.5f" % reached_precision)
            )
            self.timer.set(
                "{} secs".format("%.4f" % float(time.time() - self.time_start))
            )
            if reached_precision <= precision:
                self.root.after_idle( self.calcul_value)
            if reached_precision >= precision:
                self.suite_calcul = 1
                self.iteration = 0
                self.calc_value = 0
                delta_calc_pi = 0
                reached_precision = 0.0
                self.init = 0


class Syracuse:
    """
    Syracuse game
    """

    def __init__(self, root):
        self.root = root
        self.reached_range = StringVar()
        self.higher_value = StringVar()
        self.now_value = StringVar()
        self.timer = StringVar()
        self.begin_value = StringVar()
        self.time_start = time.time()
        self.altitude = 0
        self.altitude_max = 0
        self.distance = 0
        self.init = 0
        self.place_widget()

    def place_widget(self):
        """
        Put widget
        """
        frame = LabelFrame(
            self.root, text="Suite de Syracuse", fg="white", bg="black"
        )
        labelinit = Label(frame, text="Initial Value", fg="white", bg="black")
        entry = Entry(frame, textvariable=self.begin_value)
        button = Button(
            frame, text="Submit", bg="#333333", command=self.syracuse
        )
        button.grid(column=0, row=1, columnspan=2)
        label_distance = Label(
            frame, text="Current distance", fg="white", bg="black"
        )
        entrydistance = Entry(frame, textvariable=self.reached_range)
        labelaltitude = Label(frame, text="alt.max", fg="white", bg="black")
        entryaltitude = Entry(frame, textvariable=self.higher_value)
        labelvalue = Label(
            frame, text="Current altitude", fg="white", bg="black"
        )
        entryvalue = Entry(frame, textvariable=self.now_value)
        labeltime = Label(frame, text="time (secs):", fg="white", bg="black")
        entrytime = Entry(frame, textvariable=self.timer)
        frame.grid(column=2, row=0)
        labelinit.grid(column=0, row=0)
        entry.grid(column=1, row=0)
        label_distance.grid(column=0, row=2)
        entrydistance.grid(column=1, row=2)
        labelaltitude.grid(column=0, row=3)
        labelvalue.grid(column=0, row=4)
        entryaltitude.grid(column=1, row=3)
        entryvalue.grid(column=1, row=4)
        labeltime.grid(column=0, row=5)
        entrytime.grid(column=1, row=5)
        self.higher_value.set("0")
        self.reached_range.set("0")
        self.now_value.set("0")
        self.timer.set("0 secs")

    def syracuse(self):
        """
        check syracuse conjection
        """
        if not self.init:

            try:
                if int(self.begin_value.get()) > 0:
                    self.altitude = int(self.begin_value.get())
                    self.init = 1
                    self.time_start = time.time()
                    self.root.after(10, self.syracuse)
                else:
                    self.begin_value.set("Please enter positive integer")
            except ValueError:
                self.begin_value.set("Please enter integer")

        else:
            if self.altitude % 2 == 0:
                self.altitude = self.altitude / 2
            else:
                self.altitude = 3 * self.altitude + 1
            if self.altitude > self.altitude_max:
                self.altitude_max = self.altitude
            self.distance += 1
            self.higher_value.set(int(self.altitude_max))
            self.reached_range.set(self.distance)
            self.now_value.set(int(self.altitude))
            self.timer.set(
                "{} secs".format("%.4f" % float(time.time() - self.time_start))
            )
            if self.altitude == 1 or self.altitude == 0 or self.altitude == -1:
                self.init = 0
                self.altitude = 0
                self.altitude_max = 0
                self.distance = 0
            else:
                self.root.after_idle( self.syracuse)
