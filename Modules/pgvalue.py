"""
Library to manage pg things
"""
from tkinter import StringVar, LabelFrame, Label, Entry, Button
from itertools import count, islice
import math
import time


class Pg:
    """
    Define common GUI
    """

    def __init__(self, root):
        """
        Common GUI
        """
        self.firstnumber = StringVar()
        self.secondnumber = StringVar()
        self.result = StringVar()
        self.framepg = LabelFrame(
            root, text="GCD calcuate", width=10, fg="black", bg="yellow"
        )
        self.time = StringVar()
        Label(
            self.framepg,
            width=13,
            text="first number",
            bg="yellow",
            fg="black",
        ).grid(column=0, row=0)
        Entry(self.framepg, width=10, textvariable=self.firstnumber).grid(
            column=1, row=0
        )
        Label(
            self.framepg,
            width=13,
            text="second number",
            bg="yellow",
            fg="black",
        ).grid(column=0, row=1)
        Entry(self.framepg, width=10, textvariable=self.secondnumber).grid(
            column=1, row=1
        )
        Button(
            self.framepg,
            text="Go",
            bg="orange",
            fg="black",
            command=self.get_result,
        ).grid(column=0, row=2, columnspan=2)
        Label(self.framepg, text="RESULT", bg="yellow", fg="black").grid(
            column=0, row=3
        )
        Entry(self.framepg, width=10, textvariable=self.result).grid(
            column=1, row=3
        )
        Label(
            self.framepg, text="Time", bg="yellow", fg="black"
        ).grid(column=0, row=4)
        Entry(self.framepg,width=8,textvariable=self.time).grid(column=1,row=4)
        Label(self.framepg, text="secs", bg="yellow", fg="black").grid(column=2, row=4)
        self.time.set("00")
        self.tune_frame()

    def isprime(self, x):
        if x >= 2:
            for y in range(2, int(math.sqrt(x) - 1)):
                if not (x % y):
                    return False
        else:
            return True
        return True

    def get_result(self):
        """
        nothin to do
        """

    def tune_frame(self):
        """
        nothing
        to do
        """


class Pgcd(Pg):
    """
    PGCD class
    """

    def tune_frame(self):
        """
        Tune the frames
        """
        print(self.framepg)
        self.framepg.grid(column=0, row=2)

    def get_result(self):
        """
        display
        the
        result
        """
        timestart = time.time()
        first = int(self.firstnumber.get())
        second = int(self.secondnumber.get())
        liste = [x for x in range(1, min(first, second) + 1)]

        for i in liste[::-1]:
            if self.isprime(first) or self.isprime(second):
                print(liste[::-1])
                if max(first, second) % min(first, second) == 0:
                    self.result.set(min(first, second))
                else:
                    self.result.set(1)
                break
            if first % i == 0:
                if second % i == 0:
                    self.result.set(i)
                    self.time.set("%.4f" % float(time.time() - timestart)
                        )
                    break




class Ppcm(Pg):
    """
    PPCM Class
    """

    def tune_frame(self):
        print(self.framepg)
        self.framepg.config(text="PPCM")
        self.framepg.grid(column=2, row=2)

    def get_result(self):
        self.result == 0
        time_start = time.time()
        print("Entree PPCM")
        first = int(self.firstnumber.get())
        second = int(self.secondnumber.get())
        mini = max(first, second)
        maxi = first * second
        table = [x for x in range(mini, maxi) if not self.isprime(x)]
        for i in table:
            if i % first == 0 and i % second == 0:
                self.result.set(i)
                break
        self.result.set(maxi)
        self.time.set("%.4f" % float(time.time() - time_start))
